## Instalação do projeto

1. Clone o repositório para a pasta da aplicação na sua máquina;
2. Navegue até a pasta aplicação e execute o comando `composer install`;
3. Altere as credenciais do banco de dados no arquivo `.env`;
4. Execute o comando `php artisan migrate` no terminal;
5. Ainda no terminal execute o comando `php artisan tinker`;
6. Execute o comando a seguir no `tinker`alterando os seus dados para realizar o login como administrador e cadastrar outros usuários:
    `User::create(['name' => 'NOME DO USUÁRIO', 'last_name' => 'SOBRENOME DO USUÁRIO', 'phone_number' => 'NÚMERO DE TELFONE', 'is_admin' => 1, 'email' => 'E-MAIL DO USUÁRIO', 'password'=> Hash::make('SENHA DO USUÁRIO')]);`
7. No terminal, saia do `tinker`e execute o comando `php artisan serve` para subir o servidor da aplicação;
8. Clique no link que será exibido no terminal.