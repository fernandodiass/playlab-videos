@extends('layout.base')

@section('content')
    <div class="col-md-8 offset-2">


        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Informações do usuário</h3>
            </div>

            <form method="POST" action="{{ $route }}">
                @csrf
                @isset($user)
                    @method('put')
                @endisset
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Primeiro nome</label>
                                <input type="text" class="form-control" placeholder="Ex: João" name="name" required
                                    value="{{ isset($user) ? $user->name : null }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="last_name">Sobrenome</label>
                                <input type="text" class="form-control" placeholder="Ex: Silva" name="last_name" required
                                    value="{{ isset($user) ? $user->last_name : null }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <label for="phone_number">Telefone</label>
                                <input type="text" class="form-control" placeholder="(11)1234-5678" name="phone_number"
                                    required value="{{ isset($user) ? $user->phone_number : null }}">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="is_admin">Permissão</label>
                                <select name="is_admin" id="is_admin" class="form-control" required>
                                    <option value="0" {{ isset($user) && $user->is_admin == 0 ? 'selected' : null }}>
                                        Comum
                                    </option>
                                    <option value="1" {{ isset($user) && $user->is_admin == 1 ? 'selected' : null }}>
                                        Administrador</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="email">E-mail</label>
                                <input type="email" class="form-control" placeholder="joao.silva@email.com.br" required
                                    name="email" value="{{ isset($user) ? $user->email : null }}">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="password">Senha</label>
                                <input type="password" class="form-control" placeholder="****" required name="password"
                                    value="">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </div>

            </form>
        </div>


    </div>
@endsection
