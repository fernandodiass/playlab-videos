@extends('layout.base')

@section('content')
    <div class="col-md-12">


        @if (session()->has('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Muito bom!</h5>
                {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h5><i class="icon fas fa-check"></i> Ops...</h5>
                {{ session('error') }}
            </div>
        @endif

        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Usuários cadastrados</h3>
            </div>

            <div class="card-body p-0">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Nome completo</th>
                            <th>E-mail</th>
                            <th>Telefone</th>
                            <th>Permissão</th>
                            <th>Data de cadastro</th>
                            <td style="width: 15%"></td>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>{{ $user->name . ' ' . $user->last_name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->phone_number }}</td>
                                <td>{{ $user->type == 0 ? 'Comum' : 'Admin' }}</td>
                                <td>{{ $user->created_at }}</td>
                                <td>
                                    <a href="{{ route('user.edit', $user->id) }}" class="btn btn-primary btn-xs"><i
                                            class="fas fa-user-edit"></i>
                                        Editar</a>
                                    <form action="{{ route('user.destroy', $user->id) }}" method="POST" style="display: inline-block">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-xs"><i class="fas fa-trash"></i>
                                            Excluir</button>
                                    </form>

                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="7" class="text-center">Nenhum usuário cadastrado ainda :(</td>
                            </tr>
                        @endforelse


                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
