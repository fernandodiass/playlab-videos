@extends('layout.base')

@section('content')
<section class="content-header">
    <div class="container-fluid">
    <h2 class="text-center display-4">Buscar vídeos</h2>
    </div>

    </section>
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <form action="/" method="GET">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="maxResults">Quantidade</label>
                            <select name="maxResults" id="maxResults" class="form-control">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="15">15</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group input-group-lg">
                        <input type="search" class="form-control form-control-lg" placeholder="Digite o termo para busca"
                            name="q" />
                        <div class="input-group-append">
                            <a href="#" class="btn btn-lg btn-default" id="handle_search">
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @isset($items)
        <div class="row">
            <div class="col-lg-10 offset-md-1">
                <div class="card" style="position: relative; left: 0px; top: 0px;">
                    <div class="card-header">
                        <h3 class="card-title">
                            <i class="fab fa-youtube"></i>
                            Vídeos
                        </h3>
                    </div>
                    <div class="card-body">
                        @foreach ($items->chunk(4) as $chunk)
                            <div class="row mb-5">
                                @foreach ($chunk as $item)
                                    <div class="col-lg-3">
                                        <img src="{{ $item->snippet->thumbnails->medium->url }}" alt="">
                                        <h5>{{ $item->snippet->title }}</h5>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endisset
@endsection

