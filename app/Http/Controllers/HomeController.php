<?php

namespace App\Http\Controllers;

use App\Models\YoutubeHistorySearch;
use Google\Service\YouTube;
use Google_Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $page_name = 'Vídeos';
        try {
            YoutubeHistorySearch::create(['term' => $request->q, 'user_id' => Auth::user()->id]);
            $DEVELOPER_KEY = 'AIzaSyDQf4EojkJJUeMDpOEK12cZZ7SNLudvoA4';

            $client  = new Google_Client();
            $client->setDeveloperKey($DEVELOPER_KEY);

            $youtube = new YouTube($client);

            $searchResponse = $youtube->search->listSearch('id,snippet', array(
                'q' => $request->q,
                'maxResults' => $request->maxResults,
            ));

            $collection = collect($searchResponse->items);

            return view('index', ['page_name' => $page_name, 'collection' => $collection]);

        } catch (\Exception $e) {
            return view('index', ['page_name' => $page_name]);
        }
    }
}
