<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $page_name = "Usuários";
        $users = User::all();
        return view('user.index', array('users' => $users, 'page_name' => $page_name));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $route = route('user.store');
        $page_name = "Novo Usuário";


        return view('user.create', array('route' => $route,  'page_name' => $page_name));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        try {

            $user = User::create([
                'name' => $request->name,
                'last_name' => $request->last_name,
                'phone_number' => $request->phone_number,
                'is_admin' => $request->is_admin,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            return redirect()->route('user.index')->with('success', 'Usuário criado com sucesso!');
        } catch (\Exception) {

            return redirect('user.index')->with('error', 'Erro ao tentar criar usuário. Não se preocupe, nossa equipe foi notificada.');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        $user->where('id', $user->id)->get();
        $route = route('user.update', $user->id);
        $page_name = "Editar Usuário";

        return view('user.create', array('user' => $user, 'route' => $route, 'page_name' => $page_name));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user)
    {
        try {
            User::where('id', $user->id)->update([
                'name' => $request->name,
                'last_name' => $request->last_name,
                'phone_number' => $request->phone_number,
                'is_admin' => $request->is_admin,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            return redirect()->route('user.index')->with('success', 'Usuário alterado com sucesso!');


            return view('user.create');
        } catch (\Exception $e) {

            return redirect()->route('user.index')->with('error', 'Erro ao tentar alterar usuário. Não se preocupe, nossa equipe foi notificada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        try {

            User::destroy($user->id);

            return redirect()->route('user.index')->with('success', 'Usuário removido com sucesso!');


            return view('user.create');
        } catch (\Exception $e) {

            return redirect()->route('user.index')->with('error', 'Erro ao tentar remover usuário. Não se preocupe, nossa equipe foi notificada.');
        }
    }
}
